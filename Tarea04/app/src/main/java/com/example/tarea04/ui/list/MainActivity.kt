package com.example.tarea04.ui.list

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.tarea04.R
import com.example.tarea04.model.Contacto
import com.example.tarea04.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding : ActivityMainBinding
    private lateinit var adapter : ContactoAdapter
    private var contactos: List<Contacto> = listOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        configureAdapter()
        loadData()
    }

    private fun configureAdapter() = with(binding){
        adapter = ContactoAdapter()
        binding.rvListContacto.adapter = adapter
    }

    private fun loadData() {
        contactos = listOf(
            Contacto("Paúl Sánchez Urbano", "Full Stack Developer", "sanchez.u.1992@gmail.com"),
            Contacto("Pablo Velasquez Falcón", "Developer", "Pablo.u@gmail.com"),
            Contacto("Roberto Valera Zárate", "Front End", "valera@gmail.com"),
            Contacto("Sindy Bolaños Martin", "UX Designer", "Sindy@gmail.com"),
            Contacto("Ernesto Barrios Torres", "UX Designer", "Barrios@gmail.com"),
            Contacto("Alexander Delgado", "UX Designer", "Delgado@gmail.com"),
            Contacto("Ronie Jaramillo", "Líder Técnico", "Jaramillo@gmail.com"),
            Contacto("Anthony Torres", "Android Developer", "Anthony@gmail.com"),
        )
        adapter.updateListPokemon(contactos)
    }
}