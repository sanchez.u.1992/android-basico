package com.example.tarea04.model

import java.io.Serializable

data class Contacto(
    val name: String,
    val position: String,
    val email: String) : Serializable