package com.example.tarea04.ui.list

import android.view.LayoutInflater
import androidx.recyclerview.widget.RecyclerView
import com.example.tarea04.model.Contacto
import android.view.View
import android.view.ViewGroup
import com.example.tarea04.R
import com.example.tarea04.databinding.ItemContactoBinding

class ContactoAdapter constructor(var contactos: List<Contacto> = listOf()) : RecyclerView.Adapter<ContactoAdapter.ContactoAdapterViewHolder>() {

    lateinit var setOnClickContacto : (Contacto) -> Unit

    inner class ContactoAdapterViewHolder constructor(itemView: View): RecyclerView.ViewHolder(itemView) {
        private val binding : ItemContactoBinding = ItemContactoBinding.bind(itemView)

        fun bind(contacto: Contacto) = with(binding) {
            tvInitial.text = contacto.name.substring(0, 1).uppercase()
            tvName.text = contacto.name
            tvPosition.text = contacto.position
            tvEmail.text = contacto.email

            root.setOnClickListener {
                setOnClickContacto(contacto)
            }
        }
    }

    fun updateListPokemon(contactos : List<Contacto>){
        this.contactos = contactos
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactoAdapterViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.item_contacto,parent,false)
        return ContactoAdapterViewHolder(view)
    }

    override fun onBindViewHolder(holder: ContactoAdapterViewHolder, position: Int) {

        val contacto : Contacto = contactos[position]
        holder.bind(contacto)

    }

    override fun getItemCount(): Int {
        return contactos.size
    }
}