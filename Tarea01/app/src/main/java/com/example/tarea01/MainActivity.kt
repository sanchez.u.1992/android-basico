package com.example.tarea01

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnProcesar.setOnClickListener {
            val age = etEdad.text.toString()
            var result = "Usted es mayor de edad"

            if (age.toInt() in 1..17) {
                result = "Usted es menor de edad"
            }

            tvResultado.text = result
        }
    }
}