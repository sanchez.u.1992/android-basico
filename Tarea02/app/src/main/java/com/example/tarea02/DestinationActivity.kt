package com.example.tarea02

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_destination.*
import kotlinx.android.synthetic.main.activity_main.*
import java.util.ArrayList

class DestinationActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_destination)

        val bundle = intent.extras

        bundle?.let {
            val namePet = it.getString("KEY_NAME_PET") ?: "Desconocido"
            val agePet = it.getString("KEY_AGE_PET") ?: "Desconocido"
            val typePet = it.getString("KEY_TYPE_PET") ?: "Desconocido"
            val listVaccine = it.getStringArrayList("KEY_VACCINE").toString()

            if (typePet == "Perro") {
                ivPet.setImageResource(R.drawable.dog)
            }
            if (typePet == "Gato") {
                ivPet.setImageResource(R.drawable.cat)
            }
            if (typePet == "Conejo") {
                ivPet.setImageResource(R.drawable.rabbit)
            }

            tvNamePetResult.text = namePet
            tvAgePetResult.text = agePet
            tvTypePetResult.text = typePet
            tvVaccineResult.text = listVaccine.replace("[", "").replace("]","")
        }
    }
}