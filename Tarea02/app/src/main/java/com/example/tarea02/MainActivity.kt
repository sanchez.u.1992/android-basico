package com.example.tarea02

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnSend.setOnClickListener {
            val namePet = etName.text.toString()
            val agePet = etAge.text.toString()
            val typePet = if (rbCan.isChecked) rbCan.text.toString() else if (rbCat.isChecked) rbCat.text.toString() else rbRabbit.text.toString()
            val listVaccine = ArrayList<String>()
            if (!chkRabia.isChecked && !chkParasitos.isChecked && !chkDistemper.isChecked) {
                toast(getString(R.string.validate_vaccine_message))
            }
            if (chkRabia.isChecked) listVaccine.add(chkRabia.text.toString())
            if (chkParasitos.isChecked) listVaccine.add(chkParasitos.text.toString())
            if (chkDistemper.isChecked) listVaccine.add(chkDistemper.text.toString())

            val bundle = Bundle().apply {
                putString("KEY_NAME_PET", namePet)
                putString("KEY_AGE_PET", agePet)
                putString("KEY_TYPE_PET", typePet)
                putStringArrayList("KEY_VACCINE", listVaccine)
            }

            val intent = Intent(this, DestinationActivity::class.java).apply {
                putExtras(bundle)
            }
            startActivity(intent)
        }
    }

    fun toast(message:String) = Toast.makeText(this,"$message", Toast.LENGTH_LONG).show()
}